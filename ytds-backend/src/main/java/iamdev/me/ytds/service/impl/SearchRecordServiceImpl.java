package iamdev.me.ytds.service.impl;

import iamdev.me.ytds.entity.SearchRecord;
import iamdev.me.ytds.mapper.SearchRecordMapper;
import iamdev.me.ytds.service.ISearchRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@Service
public class SearchRecordServiceImpl extends ServiceImpl<SearchRecordMapper, SearchRecord> implements ISearchRecordService {

}
