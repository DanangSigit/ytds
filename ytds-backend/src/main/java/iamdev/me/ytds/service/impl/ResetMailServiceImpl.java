package iamdev.me.ytds.service.impl;

import iamdev.me.ytds.entity.ResetMail;
import iamdev.me.ytds.mapper.ResetMailMapper;
import iamdev.me.ytds.service.IResetMailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@Service
public class ResetMailServiceImpl extends ServiceImpl<ResetMailMapper, ResetMail> implements IResetMailService {

}
