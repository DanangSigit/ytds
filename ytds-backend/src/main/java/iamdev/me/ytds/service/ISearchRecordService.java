package iamdev.me.ytds.service;

import iamdev.me.ytds.entity.SearchRecord;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
public interface ISearchRecordService extends IService<SearchRecord> {

}
