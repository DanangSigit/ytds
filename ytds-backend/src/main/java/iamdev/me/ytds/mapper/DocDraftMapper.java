package iamdev.me.ytds.mapper;

import iamdev.me.ytds.entity.DocDraft;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
public interface DocDraftMapper extends BaseMapper<DocDraft> {

}
