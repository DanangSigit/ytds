package iamdev.me.ytds.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@TableName("tb_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;
    /**
     * 用户名
     */
    @TableField("user_name")
    private String userName;
    /**
     * sha256来哈希用户密码（not use md5）sha256(salt+pwd)
     */
    @TableField("user_pwd")
    @JSONField(serialize=false)
    private String userPwd;
    /**
     * 用户邮箱
     */
    @TableField("user_mail")
    @JSONField(serialize=false)
    private String userMail;
    /**
     * 用户注册日期
     */
    @TableField("register_date")
    @JSONField(serialize=false)
    private Date registerDate;
    /**
     * 用户token
     */
    @TableField("user_token")
    private String userToken;
    /**
     * 加盐
     */
    @TableField("user_salt")
    @JSONField(serialize=false)
    private String userSalt;
    /**
     * 用户昵称
     */
    @TableField("nick_name")
    private String nickName;
    /**
     * 用户头像文件id
     */
    @TableField("portrait_id")
    private String portraitId;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getUserSalt() {
        return userSalt;
    }

    public void setUserSalt(String userSalt) {
        this.userSalt = userSalt;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPortraitId() {
        return portraitId;
    }

    public void setPortraitId(String portraitId) {
        this.portraitId = portraitId;
    }

    @Override
    public String toString() {
        return "User{" +
        ", userId=" + userId +
        ", userName=" + userName +
        ", userPwd=" + userPwd +
        ", userMail=" + userMail +
        ", registerDate=" + registerDate +
        ", userToken=" + userToken +
        ", userSalt=" + userSalt +
        ", nickName=" + nickName +
        ", portraitId=" + portraitId +
        "}";
    }
}
