package iamdev.me.ytds.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zxc
 * @since 2018-05-09
 */
@TableName("tb_upload_file")
@ApiModel("上传文件实体")
public class UploadFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("file_id")
    @ApiModelProperty("文件id")
    private String fileId;
    @ApiModelProperty(hidden = true)
    @TableField("file_path")
    private String filePath;
    @ApiModelProperty(hidden = true)
    private String sha256;
    @TableField("file_size")
    @ApiModelProperty("文件大小")
    private Long fileSize;
    @TableField("file_type")
    @ApiModelProperty("文件类型")
    private String fileType;
    @TableField("file_desc")
    @ApiModelProperty(hidden = true)
    private String fileDesc;
    @TableField("file_name")
    @ApiModelProperty("文件名字")
    private String fileName;
    @ApiModelProperty("文件原生名字")
    @TableField("original_file_name")
    private String originalFileName;


    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getSha256() {
        return sha256;
    }

    public void setSha256(String sha256) {
        this.sha256 = sha256;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long  fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileDesc() {
        return fileDesc;
    }

    public void setFileDesc(String fileDesc) {
        this.fileDesc = fileDesc;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    @Override
    public String toString() {
        return "UploadFile{" +
        ", fileId=" + fileId +
        ", filePath=" + filePath +
        ", sha256=" + sha256 +
        ", fileSize=" + fileSize +
        ", fileType=" + fileType +
        ", fileDesc=" + fileDesc +
        ", fileName=" + fileName +
        ", originalFileName=" + originalFileName +
        "}";
    }
}
