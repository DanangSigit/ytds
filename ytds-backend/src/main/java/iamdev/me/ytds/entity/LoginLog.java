package iamdev.me.ytds.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@TableName("tb_login_log")
public class LoginLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 登录id
     */
    @TableId(value = "login_id", type = IdType.AUTO)
    private Integer loginId;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 登录ip
     */
    @TableField("login_ip")
    private String loginIp;
    /**
     * 登录日期
     */
    @TableField("login_date")
    private Date loginDate;
    /**
     * 登录来源
     */
    @TableField("login_from")
    private String loginFrom;


    public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public String getLoginFrom() {
        return loginFrom;
    }

    public void setLoginFrom(String loginFrom) {
        this.loginFrom = loginFrom;
    }

    @Override
    public String toString() {
        return "LoginLog{" +
        ", loginId=" + loginId +
        ", userId=" + userId +
        ", loginIp=" + loginIp +
        ", loginDate=" + loginDate +
        ", loginFrom=" + loginFrom +
        "}";
    }
}
